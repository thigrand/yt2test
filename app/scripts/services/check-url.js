'use strict';

function checkAnchor(validator, $q, vimeoVideo, youtubeVideo) {

	var vendorObj = {
		vendor: undefined,
		id: undefined
	};
	var VIMEO_REGEX = 'vimeo';
	var YOUTUBE_REGEX = 'youtu';

	var vendors = [vimeoVideo, youtubeVideo];
	var vendorsreg = [VIMEO_REGEX, YOUTUBE_REGEX];

	var deferred = $q.defer();

	function checkUrl(url) {

    if(isID(url)) {
    	vendorObj.vendor = checkID(url);
    	if(vendorObj.vendor){
    		vendorObj.id = url;
    	}
    } else {

    	vendorsreg.some(function(temp) {
        var reg = new RegExp(temp);
        if(reg.test(url)){
        	vendorObj.vendor = temp;
        }
        return reg.test(url);	
    	})

    	takeIdFromUrl(url);
    }

    var result = vendorObj.vendor ? vendorObj : -1;

    return $q.resolve(result);
	}

	function isID(url){
		return ((url.length < 12) && (url.length >6)) ? true : false;
	}
	function checkID(url){
		return validator.type(url);
	}

	function takeIdFromUrl(url) {

  	vendors.some(function(temp) {
      var reg = new RegExp(temp.kindsh);
      if(reg.test(url)){
      	vendorObj.id = temp.takeIdFromUrl(url);
      }
      return reg.test(url);	
  	})
	}

	return {
		checkUrl: checkUrl,
	};
}
angular.module('ytApp').factory('checkAnchor', ['validator', '$q', 'vimeoVideo', 'youtubeVideo', checkAnchor]);

